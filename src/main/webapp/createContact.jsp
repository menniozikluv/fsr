<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>   
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Create a new contact</title>
</head>
<body>
<c:url var="post_url"  value="/CreateContact" />
<sf:form method="post" action="${post_url}">
		<table>
			<tr>
				<th><h2>Cr�er un contact</h2></th>
				<tr>
					<td><i>First name: <input type="text" name="firstName" size="25"></i></td>
				</tr>
				<tr>
					<td><i>Last name: <input type="text" name="lastName" size="25"></i></td>
				</tr>
				<tr>
					<td><i>email: <input type="text" name="email" size="25"></i></td>
				</tr>
				<tr>
					<td><i>street : <input type="text" name="street" size="25"></i></td>
				</tr>
				<tr>
					<td><i>city: <input type="text" name="city" size="25"></i></td>
				</tr>
				<tr>
					<td><i>zip: <input type="text" name="zip" size="25"></i></td>
				</tr>
				<tr>
					<td><i>country: <input type="text" name="country" size="25"></i></td>
				</tr>
				<tr>
					<td><i>phoneKind1: <input type="text" name="kind1" size="25"></i></td>
				</tr>
				<tr>
					<td><i>number1: <input type="text" name="num1" size="25"></i></td>
				</tr>
				<tr>
					<td><i>phoneKind2: <input type="text" name="kind2" size="25"></i></td>
				</tr>
				<tr>
					<td><i>number2: <input type="text" name="num2" size="25"></i></td>
				</tr>
				<tr>
					<td><input class="button" type="submit" value="Submit" /><input class="button" type="reset" value="Reset"></td>
				</tr>
			
		</table>
	</sf:form>
</body>
</html>