package com.lip6.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.lip6.entities.Contact;
import com.lip6.entities.ContactGroup;
import com.lip6.services.ContactGroupService;

/**
 * Servlet implementation class HomeContactGroupServlet
 */

@Component
public class HomeContactGroupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     ContactGroupService service;
     ApplicationContext context;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeContactGroupServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init() {
		context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		service = context.getBean("cgservice", ContactGroupService.class);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<ContactGroup> contactGroups = service.getAllContactGroup(); 
		JSONArray arrayContactGroup = new JSONArray();
		for(ContactGroup contactGr : contactGroups) {
			JSONArray arrayGroup = new JSONArray();
			arrayGroup.put(contactGr.getGroupeName());
			for(Contact contact: contactGr.getContacts()) {
				JSONObject objContact = new JSONObject();
				objContact.put("email",contact.getEmail());
				objContact.put("lastName",contact.getLastName());
				objContact.put("firstName",contact.getFirstName());
				arrayGroup.put(objContact);
			}
			arrayContactGroup.put(arrayGroup);
		}
		
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(arrayContactGroup.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		StringBuilder sb = new StringBuilder();
	    String line;
	    try {
	        BufferedReader reader = request.getReader();
	        while ((line = reader.readLine()) != null) {
	            sb.append(line);
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	    String jsonString = sb.toString();
	    JSONObject jsonObject = new JSONObject(jsonString);

	    String action = jsonObject.getString("action");
	    String groupName = jsonObject.getString("groupName");
		
		Contact contact;
		ContactGroup group;
		boolean bool;
		JSONObject jsonResponse = new JSONObject();
		
		switch(action) {
		case "ajouter":
			group = service.getGroup(groupName);
			request.getSession().setAttribute("GroupWithContact", group);
			if(group != null) {
				jsonResponse.put("Groupe mit en session", true);
			}else {
				jsonResponse.put("erreur lors de la mise en session", true);
			}
			break;
		case "supprimer":
			bool = service.deleteContactGroup(groupName);
			if(bool) {
				jsonResponse.put("Groupe supprimer", true);
			}else {
				jsonResponse.put("Echec de la suppression du groupe", false);
			}
			break;
		case "retirer":
			contact = context.getBean("ccontact",Contact.class);
			JSONObject contactJson = jsonObject.getJSONObject("contact");
		    
			contact.setEmail(contactJson.getString("email"));
			contact.setLastName(contactJson.getString("lastName"));
			contact.setFirstName(contactJson.getString("firstName"));
			contact.setIdContact(0L);
			bool = service.removeContactFromGroup(groupName, contact);
			if(bool) {
				jsonResponse.put("Contact retirer du Groupe", true);
			}else {
				jsonResponse.put("Le contact n'a pas pu etre retirer du groupe", false);
			}
			break;
		case "creer":
			group = context.getBean("ccontactgroup",ContactGroup.class);
			group.setIdContactGroup(0L);
			group.setGroupeName(groupName);
			group.getContacts().clear();
			bool = service.createGroup(group);
			if(bool) {
				jsonResponse.put("Groupe créer", true);
			}else {
				jsonResponse.put("Echec de la création du groupe", false);
			}
			break;
		}
		response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonResponse.toString());
	}

}
