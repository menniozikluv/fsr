package com.lip6.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.lip6.entities.Contact;
import com.lip6.entities.ContactGroup;
import com.lip6.services.ContactGroupService;

/**
 * Servlet implementation class updateContactGroupServlet
 */

@Component
public class updateContactGroupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 ContactGroupService service;
     ApplicationContext context;
     
    /**
     * @see HttpServlet#HttpServlet()
     */
    public updateContactGroupServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init() {
		context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		service = context.getBean("cgservice", ContactGroupService.class);
	}
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ContactGroup contactGroups = (ContactGroup) request.getSession().getAttribute("GroupWithContact");
		String groupName = contactGroups.getGroupeName();
		JSONArray arrayContactGroup = new JSONArray();
		JSONArray arrayContactFromGroup = new JSONArray();
			for(Contact contact: contactGroups.getContacts()) {
				JSONObject objContact = new JSONObject();
				objContact.put("email",contact.getEmail());
				objContact.put("lastName",contact.getLastName());
				objContact.put("firstName",contact.getFirstName());
				arrayContactFromGroup.put(objContact);
			}
			arrayContactFromGroup.put(groupName);
			arrayContactGroup.put(arrayContactFromGroup);

		JSONArray arrayContactNotFromGroup = new JSONArray();	
		List<Contact> contacts = service.GetOtherContact(groupName);
		for(Contact contact: contacts) {
			JSONObject objContact = new JSONObject();
			objContact.put("email",contact.getEmail());
			objContact.put("lastName",contact.getLastName());
			objContact.put("firstName",contact.getFirstName());
			arrayContactNotFromGroup.put(objContact);
		}
		arrayContactGroup.put(arrayContactNotFromGroup);
		
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(arrayContactGroup.toString());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ContactGroup contactGroups = (ContactGroup) request.getSession().getAttribute("GroupWithContact");
		Contact contact = context.getBean("ccontact",Contact.class);
		
		StringBuilder sb = new StringBuilder();
	    String line;
	    try {
	        BufferedReader reader = request.getReader();
	        while ((line = reader.readLine()) != null) {
	            sb.append(line);
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	    String jsonString = sb.toString();
	    String oldName = contactGroups.getGroupeName();
	    JSONObject jsonObject = new JSONObject(jsonString);
	    String action = jsonObject.getString("action");
	    JSONObject contactJson ;
	    JSONObject jsonResponse = new JSONObject();
	    boolean bool = false;
	    
	    switch(action) {
	    case "ajouter":
	    	contactJson = jsonObject.getJSONObject("contact");
	    	String firstName = contactJson.getString("firstName");
		    String lastName = contactJson.getString("lastName");
		    String mail = contactJson.getString("email");
			
			contact.setEmail(mail);
			contact.setLastName(lastName);
			contact.setFirstName(firstName);
			contact.setIdContact(0L);
			bool = service.updateGroup(contactGroups.getGroupeName(), contact);
	    	break;
	    case "modifier":
	    	String newgroupName = jsonObject.getString("groupName");
	    	bool = service.UpdateGroupName(newgroupName,oldName);
	    	break;
	    }
	    
		if(bool) {
			jsonResponse.put("Modification faite sur le groupe : " + contactGroups.getGroupeName(), true);
		}else {
			jsonResponse.put("Une erreur est arriver", false);
		}
		response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonResponse.toString());
		
	}

}
