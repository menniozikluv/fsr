package com.lip6.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.lip6.services.ContactService;
import com.lip6.entities.Adress;
import com.lip6.entities.Contact;
import com.lip6.entities.PhoneNumber;

/**
 * Servlet implementation class CreateContactServlet
 */

@Component
public class CreateContactServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateContactServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		
		StringBuilder sb = new StringBuilder();
	    String line;
	    try {
	        BufferedReader reader = request.getReader();
	        while ((line = reader.readLine()) != null) {
	            sb.append(line);
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	    String jsonString = sb.toString();
	    JSONObject contactJson = new JSONObject(jsonString);
	    JSONObject jsonResponse = new JSONObject();
		
		Adress adress = context.getBean("cadress",Adress.class);
		Contact contact = context.getBean("ccontact",Contact.class);
		PhoneNumber phone1 = context.getBean("cphone1",PhoneNumber.class);
		PhoneNumber phone2 = context.getBean("cphone2",PhoneNumber.class);
		adress.setCity(contactJson.getString("city"));
		adress.setCountry(contactJson.getString("country"));
		adress.setStreet(contactJson.getString("street"));
		adress.setZip(contactJson.getString("zip"));
		adress.setIdAdress(0L);
		adress.setContact(contact);
		
		phone1.setPhoneKind(contactJson.getString("kind1"));
		phone1.setPhoneNumber1(contactJson.getString("num1"));
		phone1.setIdPhoneNumber(0L);
		phone1.setContact(contact);
		
		phone2.setPhoneKind(contactJson.getString("kind2"));
		phone2.setPhoneNumber1(contactJson.getString("num2"));
		phone2.setIdPhoneNumber(0L);
		phone2.setContact(contact);
		
		contact.setEmail(contactJson.getString("email"));
		contact.setLastName(contactJson.getString("lastName"));
		contact.setFirstName(contactJson.getString("firstName"));
		contact.setAdress(adress);
		contact.setIdContact(0L);
		Set<PhoneNumber> phones = new HashSet<PhoneNumber>();
		phones.add(phone1);
		phones.add(phone2);
		contact.setPhones(phones);
		
		ContactService service = context.getBean("cservice",ContactService.class);
		boolean bool = service.createContact(contact);
		if(bool) {
			jsonResponse.put("Contact créer", true);
		}else {
			jsonResponse.put("Le contact n'a pas pu etre créer", false);
		}
		response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonResponse.toString());

	}

}
