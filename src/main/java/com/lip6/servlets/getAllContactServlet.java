package com.lip6.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.lip6.entities.Contact;
import com.lip6.entities.PhoneNumber;
import com.lip6.services.ContactService;

/**
 * Servlet implementation class getAllContactServlet
 */
@Component
public class getAllContactServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ContactService service;
	ApplicationContext context;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public getAllContactServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init() {
    	context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
    	service = context.getBean("cservice",ContactService.class); 
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Contact> contacts = service.getAllContact(); 
		JSONArray arrayContact = new JSONArray();
		for(Contact contact : contacts) {
			JSONObject objContact = new JSONObject();
			objContact.put("email",contact.getEmail());
			objContact.put("lastName",contact.getLastName());
			objContact.put("firstName",contact.getFirstName());
			arrayContact.put(objContact);
		}
		
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(arrayContact.toString());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StringBuilder sb = new StringBuilder();
	    String line;
	    try {
	        BufferedReader reader = request.getReader();
	        while ((line = reader.readLine()) != null) {
	            sb.append(line);
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	    String jsonString = sb.toString();
	    JSONObject jsonObject = new JSONObject(jsonString);
	    String action = jsonObject.getString("action");
	    JSONObject contactJson ;
	    String firstName;
	    String lastName;
	    String mail;
	    JSONObject jsonResponse = new JSONObject();
	    
		switch(action) {
		case "modifier":
			contactJson = jsonObject.getJSONObject("contact");
		    firstName = contactJson.getString("firstName");
		    lastName = contactJson.getString("lastName");
		    mail = contactJson.getString("email");
			Contact contact = service.getContact(firstName,lastName,mail);
			if(contact != null) {
				request.getSession().setAttribute("contactToUpdate", contact);
				jsonResponse.put("récupération réussite", true);
			}else {
				jsonResponse.put("La récupération du contact à échoué", false);
			}
			break;
		case "supprimer":
			contactJson = jsonObject.getJSONObject("contact");
		    firstName = contactJson.getString("firstName");
		    lastName = contactJson.getString("lastName");
		    mail = contactJson.getString("email");
			boolean bool = service.deleteContact(firstName,lastName,mail);
			if(bool) {
				jsonResponse.put("Contact supprimer", true);
			}else {
				jsonResponse.put("Echec de la suppression du contact", false);
			}
			break;
		case "double":
			boolean state = false;
			
			Contact c1 = context.getBean("contact1",Contact.class);
			for(PhoneNumber phone: c1.getPhones()) {
				phone.setIdPhoneNumber(0L);
			}
			c1.setIdContact(0L);
			c1.getAdress().setIdAdress(0L);
			state = service.createContact(c1);
			
			if(state) {
				Contact c2 = context.getBean("contact2",Contact.class);
				for(PhoneNumber phone: c2.getPhones()) {
					phone.setIdPhoneNumber(0L);
				}
				c2.setIdContact(0L);
				c2.getAdress().setIdAdress(0L);
				state = service.createContact(c2);
				if(state) {
					jsonResponse.put("2 contacts créer", true);
				}else {
					jsonResponse.put("1 contacts créer sur 2", true);
				}
			}else {
				jsonResponse.put("aucun contacts créer", false);
			}
		}
		response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonResponse.toString());
	}

}
