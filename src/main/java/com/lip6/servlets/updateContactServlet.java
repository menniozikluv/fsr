package com.lip6.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.lip6.entities.Adress;
import com.lip6.entities.Contact;
import com.lip6.entities.PhoneNumber;
import com.lip6.services.ContactService;

/**
 * Servlet implementation class updateContactServlet
 */
@Component
public class updateContactServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ContactService service;
	ApplicationContext context;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public updateContactServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init() {
    	context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
    	service = context.getBean("cservice",ContactService.class); 
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Contact contact = (Contact) request.getSession().getAttribute("contactToUpdate");
		if (contact != null) {
			PhoneNumber phone1 = null;
			PhoneNumber phone2 = null;
			int val = 0;
			for(PhoneNumber phone: contact.getPhones()) {
				if(val == 0) {
					phone1 = phone;
					val = 1;
				}else {
					phone2 = phone;
					val = 0;
				}
			}
			
			
			JSONArray arrayContact = new JSONArray();
			JSONArray arrayAdress = new JSONArray();
			JSONArray arrayNum1 = new JSONArray();
			JSONArray arrayNum2 = new JSONArray();
				JSONObject objContact = new JSONObject();
				JSONObject objAdress = new JSONObject();
				JSONObject objPhone1 = new JSONObject();
				JSONObject objPhone2 = new JSONObject();
				objContact.put("firstName", contact.getFirstName());
				objContact.put("lastName", contact.getLastName());
				objContact.put("email", contact.getEmail());
				
				objAdress.put("city", contact.getAdress().getCity());
				objAdress.put("country", contact.getAdress().getCountry());
				objAdress.put("street", contact.getAdress().getStreet());
				objAdress.put("zip", contact.getAdress().getZip());
				arrayAdress.put(objAdress);
				
				objPhone1.put("kind1", phone1.getPhoneKind());
				objPhone1.put("num1", phone1.getPhoneNumber1());
				arrayNum1.put(objPhone1);
				objPhone2.put("kind1", phone2.getPhoneKind());
				objPhone2.put("num1", phone2.getPhoneNumber1());
				arrayNum2.put(objPhone2);
				
				objContact.put("Adress", arrayAdress);
				objContact.put("tel1", arrayNum1);
				objContact.put("tel2", arrayNum2);
				arrayContact.put(objContact);

				response.setContentType("application/json");
			    response.setCharacterEncoding("UTF-8");
			    response.getWriter().write(arrayContact.toString());
		} else {
		    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		    response.getWriter().println("Contact non trouvé dans la session");
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		StringBuilder sb = new StringBuilder();
	    String line;
	    try {
	        BufferedReader reader = request.getReader();
	        while ((line = reader.readLine()) != null) {
	            sb.append(line);
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	    String jsonString = sb.toString();
	    JSONObject contactJson = new JSONObject(jsonString);
	    JSONObject jsonResponse = new JSONObject();
		
		Adress adress = context.getBean("cadress",Adress.class);
		Contact contact = context.getBean("ccontact",Contact.class);
		PhoneNumber phone1 = context.getBean("cphone1",PhoneNumber.class);
		PhoneNumber phone2 = context.getBean("cphone2",PhoneNumber.class);
		adress.setCity(contactJson.getString("city"));
		adress.setCountry(contactJson.getString("country"));
		adress.setStreet(contactJson.getString("street"));
		adress.setZip(contactJson.getString("zip"));
		adress.setIdAdress(0L);
		adress.setContact(contact);
		
		phone1.setPhoneKind(contactJson.getString("kind1"));
		phone1.setPhoneNumber1(contactJson.getString("num1"));
		phone1.setIdPhoneNumber(0L);
		phone1.setContact(contact);
		
		phone2.setPhoneKind(contactJson.getString("kind2"));
		phone2.setPhoneNumber1(contactJson.getString("num2"));
		phone2.setIdPhoneNumber(0L);
		phone2.setContact(contact);
		
		contact.setEmail(contactJson.getString("email"));
		contact.setLastName(contactJson.getString("lastName"));
		contact.setFirstName(contactJson.getString("firstName"));
		contact.setAdress(adress);
		contact.setIdContact(0L);
		Set<PhoneNumber> phones = new HashSet<PhoneNumber>();
		phones.add(phone1);
		phones.add(phone2);
		contact.setPhones(phones);
		
		Contact oldContact = (Contact) request.getSession().getAttribute("contactToUpdate");
		
		boolean bool = service.updateContact(oldContact, contact);
		if(bool) {
			jsonResponse.put("Contact modifier", true);
		}else {
			jsonResponse.put("Le contact n'a pas pu etre modifier", false);
		}
		response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonResponse.toString());
	}

}
