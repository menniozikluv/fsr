package com.lip6.util;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

public class JsonUtil {
	
	public static JSONObject readJsonFromRequest(HttpServletRequest request) throws IOException {
        StringBuilder jsonString = new StringBuilder();
        String line;
        BufferedReader reader = request.getReader();
        while ((line = reader.readLine()) != null) {
            jsonString.append(line);
        }
        
        return new JSONObject(jsonString.toString());
    }
}
