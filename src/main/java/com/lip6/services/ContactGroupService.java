package com.lip6.services;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lip6.daos.DAOContactGroup;
import com.lip6.entities.Contact;
import com.lip6.entities.ContactGroup;

@Service
public class ContactGroupService implements IContactGroupService {

	@Autowired
	DAOContactGroup dao;
	
	public boolean createGroup(ContactGroup group) {
		return dao.CreateContactGroup(group);
	}
	
	public ContactGroup getGroup(String groupName) {
		return dao.getGroupWithContact(groupName);
	}
	
	public List<ContactGroup> getAllContactGroup(){
		return dao.getAllGroupWithContact();
	}
	
	public List<Contact> GetOtherContact(String GroupName){
		return dao.ContactNotFromGroup(GroupName);
	}
	
	public boolean updateGroup(String GroupName,Contact contact) {
		return dao.updateGroup(GroupName,contact);
	}
	
	public boolean deleteContactGroup(String groupName) {
		return dao.deleteContactGroup(groupName);
	}
	
	public boolean removeContactFromGroup(String groupName,Contact contact) {
		return dao.deleteContact(groupName,contact);
	}

	public boolean UpdateGroupName(String newGroup, String oldGroup) {
		return dao.updateGroupName(newGroup,oldGroup);
	}
}