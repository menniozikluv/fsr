package com.lip6.services;

import java.util.List;

import com.lip6.entities.Contact;
import com.lip6.entities.ContactGroup;

public interface IContactGroupService {

	public boolean createGroup(ContactGroup group);
	public ContactGroup getGroup(String groupName);
	public List<ContactGroup> getAllContactGroup();
	public List<Contact> GetOtherContact(String GroupName);
	public boolean updateGroup(String GroupName,Contact contact);
	public boolean deleteContactGroup(String groupName);
	public boolean removeContactFromGroup(String groupName,Contact contact);
	public boolean UpdateGroupName(String newGroup, String oldGroup);
}
