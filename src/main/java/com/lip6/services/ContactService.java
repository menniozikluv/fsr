package com.lip6.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lip6.daos.DAOContact;
import com.lip6.entities.Contact;

@Service
public class ContactService implements IContactService {

	@Autowired
	DAOContact dao;
	
	public boolean createContact(Contact contact) {
		return dao.addContact(contact);
	}
	
	public Contact getContact(String firstname,String lastname,String email) {
		return dao.getContact(firstname, lastname, email);
	}
	
	public List<Contact> getAllContact(){
		return dao.getAllContact();
	}
	
	public boolean updateContact(Contact oldContact,Contact newContact) {
		return dao.updateContact(oldContact,newContact);
	}
	
	public boolean deleteContact(String firstname,String lastname,String email) {
		return dao.deleteContact(firstname, lastname, email);
	}
}
