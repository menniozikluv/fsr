package com.lip6.services;

import java.util.List;

import com.lip6.daos.DAOContact;
import com.lip6.entities.Contact;

public interface IContactService {

	
	public boolean createContact(Contact contact);
	public Contact getContact(String firstname,String lastname,String email);
	public List<Contact> getAllContact();
	public boolean updateContact(Contact oldContact,Contact newContact);
	public boolean deleteContact(String firstname,String lastname,String email);
}
