package com.lip6.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
@NamedQuery(
	    name = "getContact",
	    query = "SELECT c FROM Contact c WHERE c.firstName = :firstName AND c.lastName = :lastName AND c.email = :email"
	)
public class Contact {
	
	@OneToOne(cascade=CascadeType.ALL,mappedBy="contact",fetch = FetchType.EAGER)
	private Adress adress;
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy="contact",fetch = FetchType.EAGER)
	Set<PhoneNumber> phones = new HashSet<PhoneNumber>();
	
	@ManyToMany(mappedBy="contacts")
	private Set<ContactGroup> contactGroups=new HashSet<>();
	
	@Column
	private String firstName;
	@Column
	private String lastName;
	@Column
	private String email;
	@Id
	@Column
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private long idContact;
	
	public Contact(){
	}
	

	public Contact(long idContact, String firstName, String lastName, String email) {
		this(firstName, lastName, email);
		this.idContact = idContact;
	}


	public Contact(String firstName, String lastName, String email) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}


	public String getEmail(){
		return email;
	}
	
	public void setEmail(String email){
		this.email = email;
	}
	
	public String getFirstName(){
		return firstName;
	}
	
	public void setFirstName(String firstname){
		this.firstName = firstname;
	}
	
	
	public String getLastName(){
		return lastName;
	}
	
	public void setLastName(String lastname){
		this.lastName = lastname;
	}

	public long getIdContact() {
		return idContact;
	}

	public void setIdContact(long idContact) {
		this.idContact = idContact;
	}


	@Override
	public String toString() {
		return "Contact [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", idContact="
				+ idContact + "]";
	}


	public Set<PhoneNumber> getPhones() {
		return phones;
	}


	public void setPhones(Set<PhoneNumber> phones) {
		this.phones = phones;
	}


	public Adress getAdress() {
		return adress;
	}


	public void setAdress(Adress adress) {
		this.adress = adress;
	}


	public Set<ContactGroup> getContactGroups() {
		return contactGroups;
	}


	public void setContactGroups(Set<ContactGroup> contactGroups) {
		this.contactGroups = contactGroups;
	}	
	
	
	
}
