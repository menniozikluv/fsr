package com.lip6.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class Adress {
	
	@Column
	private String street;
	@Column
	private String city;
	@Column
	private String zip;
	@Column
	private String country;
	@Id
	@Column
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private long idAdress;
	
	@OneToOne()
	@JoinColumn(name="id_contact")
	private Contact contact;
	
	public Adress() {
	}
	
	public Adress(String street, String city, String zip, String country, long idAdress) {
		this(street, city, zip, country);
		this.idAdress = idAdress;
	}


	public Adress(String street, String city, String zip, String country) {
		super();
		this.street = street;
		this.city = city;
		this.zip = zip;
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public long getIdAdress() {
		return idAdress;
	}

	public void setIdAdress(long idAdress) {
		this.idAdress = idAdress;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}
	
	
}

