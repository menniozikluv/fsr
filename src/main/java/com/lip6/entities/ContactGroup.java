package com.lip6.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table
public class ContactGroup {
	
	@Column
	private String groupeName;
	@Id
	@Column
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private long idContactGroup;
	
	
	@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.PERSIST)
	@JoinTable(name="CTC_GRP",
	joinColumns=@JoinColumn(name="GRP_ID"),
	inverseJoinColumns=@JoinColumn(name="CTC_ID"))
	private Set<Contact> contacts=new HashSet<Contact>();
	
	
	public ContactGroup(){
	}
	

	public ContactGroup(String groupeName,long idContactGroup) {
		this(groupeName);
		this.idContactGroup = idContactGroup;
	}


	public ContactGroup(String groupeName) {
		super();
		this.groupeName = groupeName;
	}


	public String getGroupeName() {
		return groupeName;
	}


	public void setGroupeName(String groupeName) {
		this.groupeName = groupeName;
	}


	public long getIdContactGroup() {
		return idContactGroup;
	}


	public void setIdContactGroup(long idContactGroup) {
		this.idContactGroup = idContactGroup;
	}


	public Set<Contact> getContacts() {
		return contacts;
	}


	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}
	
	
}
