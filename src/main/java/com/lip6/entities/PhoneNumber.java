package com.lip6.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ManyToAny;

@Entity
@Table
public class PhoneNumber {

	@ManyToOne
	@JoinColumn(name="id_contact")
	private Contact contact;
	
	@Column
	private String phoneKind;
	@Column
	private String phoneNumber1;
	@Id
	@Column
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private long idPhoneNumber;
	
	public PhoneNumber() {
	}
	
	public PhoneNumber(String phoneKind, String phoneNumber, long idPhoneNumber) {
		this(phoneKind, phoneNumber);
		this.idPhoneNumber = idPhoneNumber;
	}


	public PhoneNumber(String phoneKind, String phoneNumber) {
		super();
		this.phoneKind = phoneKind;
		this.phoneNumber1 = phoneNumber;
	}

	public String getPhoneKind() {
		return phoneKind;
	}

	public void setPhoneKind(String phoneKind) {
		this.phoneKind = phoneKind;
	}

	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	public long getIdPhoneNumber() {
		return idPhoneNumber;
	}

	public void setIdPhoneNumber(long idPhoneNumber) {
		this.idPhoneNumber = idPhoneNumber;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}
	
	
}
