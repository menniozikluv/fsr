package com.lip6.daos;

import java.util.List;
import java.util.Set;

import com.lip6.entities.Contact;
import com.lip6.entities.ContactGroup;

public interface IDAOContactGroup {

	public boolean CreateContactGroup(ContactGroup group);
	public ContactGroup getGroupWithContact(String groupName);
	public List<ContactGroup> getAllGroupWithContact();
	public boolean updateGroup(String GroupName,Contact contact);
	public boolean deleteContactGroup(String groupName);
	public boolean deleteContact(String groupName, Contact contact);
	List<Contact> ContactNotFromGroup(String GroupName);
	public boolean updateGroupName(String newName,String oldName);
}
