﻿package com.lip6.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lip6.entities.Contact;
import com.lip6.entities.PhoneNumber;
import com.lip6.util.JpaUtil;

@Repository
public class DAOContact implements IDAOContact {

	
	

	@Override
	public boolean addContact(Contact contact) {
		EntityManager em = JpaUtil.getEmf().createEntityManager();
		boolean success = false;

		try {
			EntityTransaction tx = em.getTransaction();
			tx.begin();
			em.persist(contact);
			tx.commit();
			em.close();
			success = true;
		} catch (Exception e) {
			e.printStackTrace();

		}
		return success;
	}
	
	@Transactional
	public boolean updateContact(Contact oldContact, Contact newContact) {
	    EntityManager em = JpaUtil.getEmf().createEntityManager();
	    EntityTransaction tx = em.getTransaction();

	    try {
	        tx.begin();
	        TypedQuery<Contact> query = em.createQuery(
	                "SELECT c FROM Contact c WHERE c.firstName = :firstName AND c.lastName = :lastName AND c.email = :email",
	                Contact.class);
	        query.setParameter("firstName", oldContact.getFirstName());
	        query.setParameter("lastName", oldContact.getLastName());
	        query.setParameter("email", oldContact.getEmail());

	        List<Contact> results = query.getResultList();
	        if (results.isEmpty()) {
	            tx.rollback();
	            return false;
	        }

	        Contact contactToUpdate = results.get(0);

	        contactToUpdate.setFirstName(newContact.getFirstName());
	        contactToUpdate.setLastName(newContact.getLastName());
	        contactToUpdate.setEmail(newContact.getEmail());
	        
	        em.merge(contactToUpdate);
	        
	        contactToUpdate.getAdress().setStreet(newContact.getAdress().getStreet());
	        contactToUpdate.getAdress().setCity(newContact.getAdress().getCity());
	        contactToUpdate.getAdress().setZip(newContact.getAdress().getZip());
	        contactToUpdate.getAdress().setCountry(newContact.getAdress().getCountry());

	        for (PhoneNumber phone : newContact.getPhones()) {
	        	phone.setContact(contactToUpdate);
	            em.merge(phone);
	        }
	        contactToUpdate.getPhones().clear();
	        contactToUpdate.getPhones().addAll(newContact.getPhones());

	        em.merge(contactToUpdate);

	        tx.commit();
	        return true;

	    } catch (Exception e) {
	        if (tx != null && tx.isActive()) {
	            tx.rollback();
	        }
	        throw e;
	    } finally {
	        if (em != null) {
	            em.close();
	        }
	    }
	}

	
	@Transactional
	public boolean deleteContact(String firstName, String lastName, String email) {
		EntityManager em = JpaUtil.getEmf().createEntityManager();
		EntityTransaction tx = em.getTransaction();
        boolean success = false;
        
        
		try {
			tx.begin();
			
			TypedQuery<Contact> queryDelete = em.createQuery(
						"SELECT c FROM Contact c WHERE c.firstName = :firstName AND c.lastName = :lastName AND c.email = :email",Contact.class);
			queryDelete.setParameter("firstName", firstName);
			queryDelete.setParameter("lastName", lastName);
			queryDelete.setParameter("email", email);
			
			List<Contact> contacts = queryDelete.getResultList();

			if (!contacts.isEmpty()) {
			    em.remove(contacts.get(0));
			    success = true;
			} 

			tx.commit();
		} catch (Exception e) {
	        e.printStackTrace();
	        if (em.isOpen()) {
	            em.getTransaction().rollback();
	        }
	        success = false; 
	    } finally {
	        if (em.isOpen()) {
	            em.close();
	        }
	    }
		return success;
        
    }

	
	public Contact getContact(String firstname, String lastname, String email) {
		EntityManager em = JpaUtil.getEmf().createEntityManager();
		Query query = em.createNamedQuery("getContact");
	        query.setParameter("firstName", firstname);
	        query.setParameter("lastName", lastname);
	        query.setParameter("email", email);

	        List<Contact> results = query.getResultList();
	        em.close();
	        if (results.isEmpty()) {
	            return null;  
	        } else {
	            return results.get(0);  
	        }
	}


	public List<Contact> getAllContact() {
		EntityManager em = JpaUtil.getEmf().createEntityManager();
		TypedQuery<Contact> query = em.createQuery("SELECT c FROM Contact c", Contact.class);
		List<Contact> contactList = query.getResultList();
		em.close();
        return contactList;
	}

}
