package com.lip6.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;

import com.lip6.entities.Contact;
import com.lip6.entities.ContactGroup;
import com.lip6.util.JpaUtil;

@Repository
public class DAOContactGroup implements IDAOContactGroup {

	@Override
	public boolean CreateContactGroup(ContactGroup group) {

		EntityManager em = JpaUtil.getEmf().createEntityManager();

		try {
			EntityTransaction tx = em.getTransaction();
			tx.begin();
			em.persist(group);
			tx.commit();
			em.close();
			return true;
		} catch (Exception e) {
			return false;

		}

	}

	public ContactGroup getGroupWithContact(String groupName) {
		EntityManager em = JpaUtil.getEmf().createEntityManager();
		String queryString = "SELECT cg FROM ContactGroup cg \r\n"
				+ "LEFT JOIN FETCH cg.contacts \r\n"
				+ "WHERE cg.groupeName = :groupName";
		
		TypedQuery<ContactGroup> query = em.createQuery(queryString, ContactGroup.class);
		query.setParameter("groupName", groupName);
		ContactGroup result = query.getSingleResult();
		em.close();
		return result;
	}

	public List<Contact> ContactNotFromGroup(String GroupName) {
		EntityManager em = JpaUtil.getEmf().createEntityManager();
		String queryString = "SELECT c FROM Contact c \r\n"
				+ "WHERE NOT EXISTS \r\n"
				+ "    (SELECT cg FROM ContactGroup cg \r\n"
				+ "     WHERE cg.groupeName = :groupName AND c MEMBER OF cg.contacts)";
		
		TypedQuery<Contact> query = em.createQuery(queryString, Contact.class);
		query.setParameter("groupName", GroupName);
		List<Contact> result = query.getResultList();
		em.close();
		return result;
	}

	public List<ContactGroup> getAllGroupWithContact() {
		EntityManager em = JpaUtil.getEmf().createEntityManager();
		TypedQuery<ContactGroup> query = em.createQuery("SELECT DISTINCT cg FROM ContactGroup cg LEFT JOIN FETCH cg.contacts", ContactGroup.class);
		List<ContactGroup> contactGroupList = query.getResultList();
		System.out.println(contactGroupList.size());
		em.close();
		return contactGroupList;
	}

	public boolean updateGroup(String GroupName, Contact contact) {
		try {
			EntityManager em = JpaUtil.getEmf().createEntityManager();
			TypedQuery<ContactGroup> groupQuery = em.createQuery("SELECT cg FROM ContactGroup cg WHERE cg.groupeName = :groupName", ContactGroup.class);
			groupQuery.setParameter("groupName", GroupName);
			ContactGroup contactGroup = groupQuery.getSingleResult();

			TypedQuery<Contact> contactQuery = em.createQuery("SELECT c FROM Contact c WHERE c.firstName = :firstName AND c.lastName = :lastName AND c.email = :email", Contact.class); 
			contactQuery.setParameter("firstName", contact.getFirstName());
			contactQuery.setParameter("lastName", contact.getLastName());
			contactQuery.setParameter("email", contact.getEmail());
			Contact Querycontact = contactQuery.getSingleResult();

			contactGroup.getContacts().add(Querycontact);
			EntityTransaction tx = em.getTransaction();
			tx.begin();
			em.persist(contactGroup);
			tx.commit();
			em.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean deleteContactGroup(String groupName) {
		try {
			EntityManager em = JpaUtil.getEmf().createEntityManager();
			TypedQuery<ContactGroup> groupQuery = em.createQuery("SELECT cg FROM ContactGroup cg WHERE cg.groupeName = :groupName", ContactGroup.class);
	        groupQuery.setParameter("groupName", groupName);
	        ContactGroup contactGroup = groupQuery.getSingleResult();
			
			EntityTransaction tx = em.getTransaction();
			tx.begin();
			em.remove(contactGroup);
			tx.commit();
			em.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean deleteContact(String groupName, Contact contact) {
		try {
			EntityManager em = JpaUtil.getEmf().createEntityManager();
	        TypedQuery<ContactGroup> groupQuery = em.createQuery("SELECT cg FROM ContactGroup cg WHERE cg.groupeName = :groupName", ContactGroup.class);
	        groupQuery.setParameter("groupName", groupName);
	        ContactGroup contactGroup = groupQuery.getSingleResult();

	        TypedQuery<Contact> contactQuery = em.createQuery("SELECT c FROM Contact c WHERE c.firstName = :firstName AND c.lastName = :lastName AND c.email = :email", Contact.class);
	        contactQuery.setParameter("firstName", contact.getFirstName());
	        contactQuery.setParameter("lastName", contact.getLastName());
	        contactQuery.setParameter("email", contact.getEmail());
	        Contact QueryContact = contactQuery.getSingleResult();

	        contactGroup.getContacts().remove(QueryContact);
	        
	        EntityTransaction tx = em.getTransaction();
			tx.begin();
	        em.merge(contactGroup);
	        tx.commit();
			em.close();

	        return true; 
	    } catch (Exception e) {
	        return false; 
	    }
	}
	
	public boolean updateGroupName(String newName,String oldName) {
		EntityManager em = JpaUtil.getEmf().createEntityManager();
		EntityTransaction tx = em.getTransaction();
        boolean success = false;
        
        
		try {
			tx.begin();
			
			TypedQuery<ContactGroup> groupQuery = em.createQuery("SELECT cg FROM ContactGroup cg WHERE cg.groupeName = :groupName", ContactGroup.class);
	        groupQuery.setParameter("groupName", oldName);
	        List<ContactGroup> group = groupQuery.getResultList();
			System.out.println(group.size() + "aaaaa");

			if (!group.isEmpty()) {
				ContactGroup groupToUpdate = group.get(0);
	            groupToUpdate.setGroupeName(newName);
	            success = true;
			} 

			tx.commit();
		} catch (Exception e) {
	        e.printStackTrace();
	        if (em.isOpen()) {
	            em.getTransaction().rollback();
	        }
	        success = false; 
	    } finally {
	        if (em.isOpen()) {
	            em.close();
	        }
	    }
		return success;
	}
}
